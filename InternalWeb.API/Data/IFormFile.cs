using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace InternalWeb.API.Data
{
    public interface IFormFile
    {
         string ContentType { get; set; }
         string ContentDisposition { get; set; }
         IHeaderDictionary Headers { get; }
        long Length { get; }
        string Name { get; }
        string FileName { get; }
        Stream OpenReadStream();
        void CopyTo(Stream target);
        // Task CopyToAsync(Stream target, CancellationToken cancellationToken = null);
    }
}