using System.Collections.Generic;
using System.Threading.Tasks;
using InternalWeb.API.Models;
using Microsoft.EntityFrameworkCore;

namespace InternalWeb.API.Data
{
    public class ArticleRepository : IArticleRepository
    {
        private readonly DataContext _context;
        public ArticleRepository(DataContext context)
        {
            _context = context;

        }

        public void Add<T>(T entity) where T : class
        {
            _context.Add(entity);
        }

        public void Delete<T>(T entity) where T : class
        {
            _context.Remove(entity);
        }

        public void Update<T>(T entity) where T : class
        {
            _context.Update(entity);
        }

        public async Task<Article> GetArticle(int id)
        {
            var article = await _context.Articles.FindAsync(id);

            return article;
        }

        public async Task<IEnumerable<Article>> GetArticles()
        {
            var articles = await _context.Articles.ToListAsync();

            return articles;
        }

        public async Task<bool> SaveAll()
        {
           return await _context.SaveChangesAsync() > 0;
        }

        public async Task<Image> GetImage(int id)
        {
            var image = await _context.Images.FirstOrDefaultAsync(i => i.Id == id);

            return image;
        }
    }
}