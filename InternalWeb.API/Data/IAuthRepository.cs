using System.Threading.Tasks;
using InternalWeb.API.Models;

namespace InternalWeb.API.Data
{
    public interface IAuthRepository
    {
         Task<User> Register(User user, string password);

         Task<User> Login(string username, string password);

         Task<bool> UserExists(string username);
    }
}