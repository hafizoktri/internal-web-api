using InternalWeb.API.Models;
using Microsoft.EntityFrameworkCore;

namespace InternalWeb.API.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base (options){}
        public DbSet<Article> Articles { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Image> Images { get; set; }
    }
}