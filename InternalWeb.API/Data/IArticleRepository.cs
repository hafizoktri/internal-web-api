using System.Collections.Generic;
using System.Threading.Tasks;
using InternalWeb.API.Models;

namespace InternalWeb.API.Data
{
    public interface IArticleRepository
    {
        void Add<T>(T entity) where T : class;
        void Update<T>(T entity) where T : class;
        void Delete<T>(T entity) where T : class;
        Task<bool> SaveAll();
        Task<IEnumerable<Article>> GetArticles();
        Task<Article> GetArticle (int id);
        Task<Image> GetImage (int id);
    }
}