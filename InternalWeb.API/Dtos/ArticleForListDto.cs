using System.Collections.Generic;
using InternalWeb.API.Models;

namespace InternalWeb.API.Dtos
{
    public class ArticleForListDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public ICollection<Image> Image { get; set; }
        public string Content { get; set; }
        public string ImageUrl { get; set; }
    }
}