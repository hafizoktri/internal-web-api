using System.Collections.Generic;
using InternalWeb.API.Models;

namespace InternalWeb.API.Dtos
{
    public class ArticleForDetailedDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public ICollection<ImageForDetailedDto> Image { get; set; }
        public string Content { get; set; }
        public string ImageUrl { get; set; }
    }
}