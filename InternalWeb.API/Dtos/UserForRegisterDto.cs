using System.ComponentModel.DataAnnotations;

namespace InternalWeb.API.Dtos
{
    public class UserForRegisterDto
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [StringLength(16, MinimumLength = 6, ErrorMessage = "Password must be between 6 to 16 characters")]
        public string Password { get; set; }
    }
}