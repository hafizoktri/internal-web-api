using System.Linq;
using AutoMapper;
using InternalWeb.API.Dtos;
using InternalWeb.API.Models;

namespace InternalWeb.API.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Article, ArticleForListDto>()
                .ForMember(dest => dest.ImageUrl, opt => {
                    opt.MapFrom(src => src.Image.FirstOrDefault(i => i.IsMain).Url);
                });
            CreateMap<Article, ArticleForDetailedDto>()
                .ForMember(dest => dest.ImageUrl, opt => {
                    opt.MapFrom(src => src.Image.FirstOrDefault(i => i.IsMain).Url);
                });
            CreateMap<ImageForCreationDto, Image>();
            CreateMap<Image, ImageForDetailedDto>();
            CreateMap<Image, ImageForReturnDto>();
        }
    }
}