namespace InternalWeb.API.UploadRepo
{
    public class FileUploadDetails
    {
        public string Filepath { get; set; }
        public string FileName { get; set; }
        public long FileLength { get; set; }
        public string FileCreatedTime { get; set; }
    }
}