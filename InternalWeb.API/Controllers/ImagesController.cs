using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using InternalWeb.API.Data;
using InternalWeb.API.Dtos;
using InternalWeb.API.Helpers;
using InternalWeb.API.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace InternalWeb.API.Controllers
{
    [Route("api/articles/{articleId}/images")]
    // [Route("api/[controller]")]
    [ApiController]
    public class ImagesController : ControllerBase
    {
        private readonly IArticleRepository _repo;
        private readonly IOptions<CloudinarySettings> _cloudinaryConfig;
        private Cloudinary _cloudinary;
        private readonly IMapper _mapper;

        public ImagesController(IArticleRepository repo, 
                IOptions<CloudinarySettings> cloudinaryConfig, IMapper mapper)
        {
            _mapper = mapper;
            _cloudinaryConfig = cloudinaryConfig;
            _repo = repo;

            Account acc = new Account(
                _cloudinaryConfig.Value.CloudName,
                _cloudinaryConfig.Value.ApiKey,
                _cloudinaryConfig.Value.ApiSecret
            );

            _cloudinary = new Cloudinary(acc);
        }

        [HttpGet("{id}", Name = "GetImage")]
        public async Task<IActionResult> GetImage(int id)
        {
            var imageFromRepo = await _repo.GetImage(id);

            var image = _mapper.Map<ImageForReturnDto>(imageFromRepo);

            return Ok(image);
        }

        [HttpPost]
        public async Task<IActionResult> AddImageForArticle(int articleId, 
                [FromForm]ImageForCreationDto imageForCreationDto)
        {
            // if (articleId != int.Parse(Image.Find(articleId).Value)
            // {
            //     return Unauthorized(); 
            // }
            
            var articleFromRepo = await _repo.GetArticle(articleId);

            var file = imageForCreationDto.File;

            var uploadResult = new ImageUploadResult();

            if (file.Length > 0)
            {
                using (var stream = file.OpenReadStream())
                {
                    var uploadParams = new ImageUploadParams()
                    {
                        File = new FileDescription(file.Name, stream),
                        Transformation = new Transformation().Width(800).Height(600).Crop("fill")
                    };

                    uploadResult = _cloudinary.Upload(uploadParams);
                }
            }

            imageForCreationDto.Url = uploadResult.Uri.ToString();
            imageForCreationDto.PublicId = uploadResult.PublicId;

            var image = _mapper.Map<Image>(imageForCreationDto);

            if (!articleFromRepo.Image.Any(i => i.IsMain))
            {
                image.IsMain = true;
            }

            articleFromRepo.Image.Add(image);

            if (await _repo.SaveAll())
            {
                var imageToReturn = _mapper.Map<ImageForReturnDto>(image);
                return CreatedAtRoute("GetImage", new {id = image.Id}, imageToReturn);
            }

            return BadRequest("could not add image");
        }

    }
}