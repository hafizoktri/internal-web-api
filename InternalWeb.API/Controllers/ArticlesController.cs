using System.Linq;
using System.Threading.Tasks;
using InternalWeb.API.Data;
using InternalWeb.API.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace InternalWeb.API.Controllers
{
    [Route("api/articles")]
    [ApiController]
    public class ArticlesController : ControllerBase
    {
        private readonly IArticleRepository _repo;
        private readonly IHostingEnvironment _host;
        private readonly IMapper _mapper;

        public ArticlesController(IArticleRepository repo, IHostingEnvironment host, IMapper mapper)
        {
            _mapper = mapper;
            _repo = repo;
            _host = host;
        }

        //GET index
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var articles = await _repo.GetArticles();

            return Ok(articles);
        }

        //GET api/articles/id
        [HttpGet("{id}")]
        public async Task<IActionResult> GetArticle(int id)
        {
            var article = await _repo.GetArticle(id);

            return Ok(article);
        }

        //POST api/articles
        [HttpPost]
        public async Task<IActionResult> Create([FromBody]Article newArticle)
        {
            if (!ModelState.IsValid)
            {
                return StatusCode(422, new
                {
                    status = "422",
                    error = true,
                    message = ModelState
                });
            }

            _repo.Add(newArticle);
            // if (image != null)
            // {
            //     var fileName = Path.Combine(_host.WebRootPath, Path.GetFileName(image.FileName));
            //     await image.CopyToAsync(new FileStream(fileName, FileMode.Create));


            // }

            if (await _repo.SaveAll())
            {
                return StatusCode(201, new
                {
                    status = "201",
                    error = false,
                    message = "success"
                });
            }

            return StatusCode(400, new
            {
                status = "400",
                error = true,
                message = "Could not create new article"
            });
        }

        //PUT api/articles/[id]
        [HttpPut("{id}")]
        public async Task<IActionResult> Edit(int id, [FromBody]Article editArticle)
        {
            var article = await _repo.GetArticle(id);

            if (article == null)
            {
                return StatusCode(400, new
                {
                    status = "400",
                    error = true,
                    message = "Bad Request"
                });
            }

            if (!ModelState.IsValid)
            {
                return StatusCode(422, new
                {
                    status = "422",
                    error = true,
                    message = ModelState
                });
            }

            article.Title = editArticle.Title;
            article.Content = editArticle.Content;

            _repo.Update(article);

            if (await _repo.SaveAll())
            {
                return StatusCode(201, new
                {
                    status = "201",
                    error = false,
                    message = "success"
                });
            }

            return StatusCode(400, new
            {
                status = "400",
                error = true,
                message = "Could not update article"
            });
        }

        //DELETE api/article/[id]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var deleteArticle = await _repo.GetArticle(id);

            if (deleteArticle == null)
            {
                return StatusCode(400, new
                {
                    status = "400",
                    error = true,
                    message = "Bad Request"
                });
            }

            _repo.Delete(deleteArticle);

            if (await _repo.SaveAll())
            {
                return StatusCode(201, new
                {
                    status = "201",
                    error = false,
                    message = "success"
                });
            }

            return StatusCode(400, new
            {
                status = "400",
                error = true,
                message = "Could not delete article"
            });
        }
    }
}