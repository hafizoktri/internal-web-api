using System.Collections.Generic;

namespace InternalWeb.API.Models
{
    public class Article
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public ICollection<Image> Image { get; set; }
        public string Content { get; set; }
    }
}